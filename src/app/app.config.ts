import { ApplicationConfig } from '@angular/core';
import { provideRouter,  withComponentInputBinding } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { AuthService } from './services/auth/auth.service';
import {LocalStorageService} from "ngx-webstorage";

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes,  withComponentInputBinding()), provideClientHydration(), AuthService, LocalStorageService],
};
