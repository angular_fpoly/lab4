import { Component, inject } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import { AuthService } from '../../services/auth/auth.service'
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  constructor(private authService: AuthService, private router : Router) {}
  ngOnInit() {
    console.log(this.authService)
  }
  login() {
    this.authService.login();
    console.log(this.authService);
    this.router.navigate(['admin'])
      .catch((err) => console.log(err))
  }
}
