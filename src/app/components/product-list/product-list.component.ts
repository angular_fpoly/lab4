import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";
import {FormsModule} from "@angular/forms";
import {StarComponent} from "../star/star.component";
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [
    MainLayoutComponent,
    FormsModule,
    StarComponent,
    RouterLink
  ],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent {
  notice!: string;
  filter = '';
  products: IProduct[] = productsData;

  handleStar (str : string) {
    this.notice = str;
  }

  search() {
    if(this.filter.length <= 0) {
      this.setProductList = productsData;
    } else if(this.getProductList.length === 0) {
      this.setProductList = productsData;
    } else {
      this.setProductList = this.getProductList.filter((product) =>
        product.productName.toLowerCase().includes(this.filter.toLowerCase())
      );
    }
  }

  set setProductList(productList: IProduct[]) {
    this.products = productList;
  }

  get getProductList() {
    return this.products
  }
}
