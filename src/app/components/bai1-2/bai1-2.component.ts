import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {RouterLink} from "@angular/router";
import {StarComponent} from "../star/star.component";
import {productsData} from "../../data";

@Component({
  selector: 'app-bai1-2',
  standalone: true,
  imports: [
    MainLayoutComponent,
    RouterLink,
    StarComponent
  ],
  templateUrl: './bai1-2.component.html',
  styleUrl: './bai1-2.component.css'
})
export class Bai12Component {
  product! : IProduct;
  ngOnInit() {
      this.product = productsData[1];
  }
}
