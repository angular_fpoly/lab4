import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    MainLayoutComponent
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent {
  constructor(private router : Router, private authService : AuthService) {}
  logout() {
    this.authService.logout();
    this.router.navigate(['login'])
      .catch((err) => console.error(err))
  }
}
