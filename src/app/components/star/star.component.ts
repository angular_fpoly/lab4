import {Component, EventEmitter, Input, NgZone, Output} from '@angular/core';

@Component({
  selector: 'app-star',
  standalone: true,
  imports: [],
  templateUrl: './star.component.html',
  styleUrl: './star.component.css'
})
export class StarComponent {
  @Input() rating!: number | string;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
  starWidth!: number;

  constructor(private zone: NgZone) {}

  onClick = () => {
    this.ratingClicked.emit(`Đánh giá của sản phẩm là ${this.rating} sao !`);
  }

  ngOnInit() {
    this.starWidth = (parseInt(this.rating as string) * 110) / 5;
  }

  ngOnChange() {
    this.starWidth = (parseInt(this.rating as string) * 110) / 5;
  }
}
