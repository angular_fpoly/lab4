import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {NgOptimizedImage} from "@angular/common";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {productsData} from "../../data";
import {StarComponent} from "../star/star.component";

@Component({
  selector: 'app-product-detail',
  standalone: true,
  imports: [
    MainLayoutComponent,
    NgOptimizedImage,
    StarComponent,
    RouterLink
  ],
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css'
})
export class ProductDetailComponent {
  product! : IProduct;
  constructor(private route : ActivatedRoute) {}

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    console.log(typeof id);
    if(id) {
      [this.product] = productsData.filter((product) => product.productId === parseInt(id));
      console.log(this.product);
    }
  }
}
