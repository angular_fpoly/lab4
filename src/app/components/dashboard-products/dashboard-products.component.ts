import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";

@Component({
  selector: 'app-dashboard-products',
  standalone: true,
    imports: [
        MainLayoutComponent
    ],
  templateUrl: './dashboard-products.component.html',
  styleUrl: './dashboard-products.component.css'
})
export class DashboardProductsComponent {

}
