interface IProduct {
 productId: number;
 productName: string;
 productCode: string;
 releaseDate: string;
 price: number;
 description: string;
 starRating: number;
 imageUrl: string;
}

interface User {
  id_user: number;
  name: string;
  email: string;
  password: string;
}
