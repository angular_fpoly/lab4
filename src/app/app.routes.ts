import {Routes} from '@angular/router';
import {MainMenuComponent} from "./components/main-menu/main-menu.component";
import {HomeComponent} from "./components/home/home.component";
import {ProductDetailComponent} from "./components/product-detail/product-detail.component";
import {ProductListComponent} from "./components/product-list/product-list.component";
import {Bai12Component} from "./components/bai1-2/bai1-2.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {LoginComponent} from "./components/login/login.component";
import {DashboardProductsComponent} from "./components/dashboard-products/dashboard-products.component";
import {authGuardGuard} from "./services/AuthGuard/auth-guard.guard";


export const routes: Routes = [
  {path: '', component: MainMenuComponent},
  {path: 'bai1', component: HomeComponent},
  {path: 'product', component: Bai12Component},
  {path: 'bai2', component: ProductListComponent},
  {path: 'bai2/:id', component: ProductDetailComponent},
  {path: 'login', component: LoginComponent},
  { path: 'admin', component: DashboardComponent, canMatch : [authGuardGuard]},
  {path: 'products', component: DashboardProductsComponent}
];
