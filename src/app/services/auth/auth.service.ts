import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from "@angular/common";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn!: boolean;

  getIsLoggedIn() {
    this.isLoggedIn = !!localStorage.getItem('isLoggedIn');
  }

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    if (isPlatformBrowser(this.platformId)) {
      this.getIsLoggedIn();
    }
  }

  login(): void {
    localStorage.setItem('isLoggedIn', 'true');
    this.isLoggedIn = true;
  }

  logout(): void {
    localStorage.removeItem('isLoggedIn');
    this.isLoggedIn = false;
  }
}
