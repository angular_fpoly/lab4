import {CanActivateFn, Router} from '@angular/router';
import {inject} from '@angular/core';
import {AuthService} from "../auth/auth.service";
import {Observable} from "rxjs";

export const authGuardGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const authService = inject(AuthService);

  if (authService.isLoggedIn) {
    return true;
  } else {
    router.navigate(['/login'])
      .catch((err) => console.log(err));
    return false;
  }
};
